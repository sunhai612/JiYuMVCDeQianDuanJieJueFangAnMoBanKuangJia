﻿

/**
 * 视图模型
 */
class $safeitemrootname$ {

    keywords: KnockoutObservable<string>;

    /**
     * 构造函数
     */
    constructor() {
        var self = this;


        self.init();
    }

    /*****************************方法区*********************************/

    /*
    *   页面加载
    */
    fnPageLoad() {
        WebUtil.ajax({
            url: formatUrl("/ControllerName/FunctionName"),
            data: {
            },
            type: "post",
            success(data) {
                
            }
        });
    }

    /*****************************事件区*********************************/


    /*
    *  初始化事件
    */
    init() {
        this.fnPageLoad();
    }
}
