﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace $safeprojectname$.IBusiness
{
    /// <summary>
    /// 枚举获取接口
    /// </summary>
    public interface IEnumContract
    {
        #region # 获取审核状态枚举集  --  List<KeyValuePair<string, string>> GetCheckStatusList()

        /// <summary>
        /// 获取审核状态枚举集
        /// </summary>
        /// <returns></returns>
        List<KeyValuePair<string, string>> GetCheckStatusList();

        #endregion
    }
}
