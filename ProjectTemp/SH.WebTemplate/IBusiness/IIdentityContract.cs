﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using $safeprojectname$.Models;
using SD.Infrastructure.DTOBase;

namespace $safeprojectname$.IBusiness
{
    public interface IIdentityContract
    {
        //命令部分
        #region # 创建用户 —— void CreateUser(string loginId, string realName...

        /// <summary>
        /// 创建用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <param name="realName">真实姓名</param>
        /// <param name="password">密码</param>
        void CreateUser(string loginId, string realName, string password);
        #endregion

        #region # 为用户分配角色 —— void SetRoles(string loginId...

        /// <summary>
        /// 为用户分配角色
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <param name="roleIds">角色Id集</param>
        void SetRoles(string loginId, List<Guid> roleIds);
        #endregion

        #region # 重置密码 —— void ResetPassword(string loginId, string newPassword)

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <param name="newPassword">新密码</param>
        void ResetPassword(string loginId, string newPassword);
        #endregion

        #region # 启用用户 —— void EnableUser(string loginId)

        /// <summary>
        /// 启用用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        void EnableUser(string loginId);
        #endregion

        #region # 停用用户 —— void DisableUser(string loginId)

        /// <summary>
        /// 停用用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        void DisableUser(string loginId);
        #endregion

        #region # 删除用户 —— void RemoveUser(string loginId)

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        void RemoveUser(string loginId);
        #endregion

        //查询部分

        #region 查询部分

        #region # 获取信息系统列表字典 —— List<KeyValuePair<string, string>> GetInfoSystems()

        /// <summary>
        /// 获取信息系统列表字典
        /// </summary>
        /// <returns>信息系统列表</returns>
        List<KeyValuePair<string, string>> GetInfoSystems();

        #endregion

        #region # 分页获取角色列表 —— PageModel<RoleInfo> GetRolesByPage(string systemNo...

        /// <summary>
        /// 分页获取角色列表
        /// </summary>
        /// <param name="systemNo">信息系统编号</param>
        /// <param name="keywords">关键字</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">页容量</param>
        /// <returns>角色列表</returns>
        PageModel<RoleModel> GetRolesByPage(string systemNo, string keywords, int pageIndex, int pageSize);

        #endregion

        #region # 获取权限列表字典 —— List<KeyValuePair<Guid,string>> GetAuthorities(string systemNo)

        /// <summary>
        /// 获取权限列表字典
        /// </summary>
        /// <param name="systemNo">信息系统编号</param>
        /// <returns>权限列表</returns>
        List<KeyValuePair<Guid, string>> GetAuthorities(string systemNo);

        #endregion

        #region # 根据角色获取权限列表 —— List<KeyValuePair<Guid,string>> GetAuthoritiesByRole(...

        /// <summary>
        /// 根据角色获取权限列表
        /// </summary>
        /// <param name="roleId">角色Id</param>
        /// <returns>权限列表</returns>
        List<KeyValuePair<Guid, string>> GetAuthoritiesByRole(Guid roleId);

        #endregion

        #region # 分页获取用户列表 —— PageModel<UserInfo> GetUsers(string systemNo...

        /// <summary>
        /// 分页获取用户列表
        /// </summary>
        /// <param name="systemNo">信息系统编号</param>
        /// <param name="keywords">关键字</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">页容量</param>
        /// <returns>用户列表</returns>
        PageModel<UserModel> GetUsers(string systemNo, string keywords, int pageIndex, int pageSize);

        #endregion

        #region # 是否存在用户 —— bool ExistsUser(string loginId)

        /// <summary>
        /// 是否存在用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <returns>是否存在</returns>
        bool ExistsUser(string loginId);

        #endregion

        #region # 获取用户角色列表 —— List<RoleModel> GetRoles(string loginId, string systemNo)

        /// <summary>
        /// 获取用户角色列表
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <param name="systemNo">信息系统编号</param>
        /// <returns>角色列表</returns>
        List<RoleModel> GetRoles(string loginId, string systemNo);

        #endregion

        #endregion
    }
}
