﻿using System.Collections.Generic;
using SD.Infrastructure.Constants;
using SD.Infrastructure.MemberShip;

namespace $safeprojectname$.IBusiness
{
    public interface IUserLoginContract
    {
        #region # 登录  --  void Login(string loginId,string password,)

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="loginId">账号</param>
        /// <param name="password">密码</param>
        LoginInfo Login(string loginId, string password);

        #endregion
        
    }
}
