﻿
var perChartModel=function (perChartDate) {
    var perChart;

    function initChart() {
        // PIE CHART
        perChart = new AmCharts.AmPieChart();

        // title of the chart
        perChart.addTitle("成品粉产量", 16); //title文本及文本字体大小

        perChart.dataProvider = perChartDate; //绑定数据源
        perChart.titleField = "Name"; //title文本绑定字段
        perChart.valueField = "Turnout"; //值绑定字段
        perChart.sequencedAnimation = true;
        perChart.startEffect = "elastic";
        perChart.innerRadius = "30%"; //饼图圆心大小
        perChart.startDuration = 2;
        perChart.labelRadius = 5;
        perChart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]kg</b> ([[percents]]%)</span>";
        // 这两个属性是3D效果属性
        perChart.depth3D = 10;
        perChart.angle = 15;

        // WRITE
        perChart.write("perChartdiv");
    }

    AmCharts.ready(initChart());
}

/**
 * 刷新比重图数据
 * @param {} timeSegment 时间段  1 近一个月 3 一个季度 6 半年 12 一年
 * @returns {} 
 */
function refurbishPerChart(element,timeSegment) {
    $(element).parent().parent().siblings('a').html($(element).text() + ' <span class="fa fa-angle-down"></span>');
    $(element).parent().addClass("active").siblings().removeClass("active");
    fnGetPerDate(timeSegment, function(data) {
        perChartModel(data);
    });

}
/**
 * 获取数据源 
 * @param {} timeSegment  时间段
 * @param {} callback     回调
 * @returns {} 
 */
function fnGetPerDate(timeSegment,callback) {
    $.ajax({
        url: formatUrl("/Home/GetPerData"),
        data: {
            timeSegment: timeSegment
        },
        type: "post",
        dataType: "json",
        success: function (data) {

            if (callback != null) {
                callback(data);
            }

        }
    });
}

/**
 * 页面初始化加载最近一个月数据
 */
refurbishPerChart(null,1);