﻿
/**
 * 图表绘制方法 
 * @param {} lineChartData 图表数据源
 * @returns {} 
 */
var lineChartmodel = function (lineChartData, materials) {
    
    var lineChart;
    
    //添加折线
    function addGraph(title,field) {
        //随机生成折线颜色
        var lineColor = '#' + ('00000' + (Math.random() * 0x1000000 << 0).toString(16)).slice(-6);
        // 折线添加
        var graph = new AmCharts.AmGraph();
        graph.type = "line";//指示线条样式  不赋值为折线  smoothedLine曲线 line折线
        graph.bullet = "round";//指示是否有折点和折点样式   triangleUp三角  square方 round圆
        graph.bulletColor = "#FFFFFF"; //指示折点颜色
        graph.useLineColorForBulletBorder = true;//指示折点是否显示
        graph.hidden = false;//指示线最初是否被隐藏
        graph.bulletBorderAlpha = 1;
        graph.bulletBorderThickness = 2;//折点边框厚度
        graph.bulletSize = 7;//折点大小
        graph.title = title;//
        graph.valueField = field;//绑定字段
        graph.lineThickness = 1;//折线粗细
        graph.lineColor = lineColor;//折线颜色  -- 随机生成 也可自定义
        graph.balloonText = "[[category]]: [[value]]kg";//折点展示文本  默认展示值  [[category]]-x轴信息: [[value]]-值
        lineChart.addGraph(graph);
    }
    //初始化图表参数
    function initChart() {

        // SERIAL CHART
        lineChart = new AmCharts.AmSerialChart();

        lineChart.dataProvider = lineChartData;//图表数据源
        lineChart.categoryField = "date";  //X轴绑定字段
        lineChart.dataDateFormat = "YYYY-MM-DD";

        // 轴线
        // X轴
        var categoryAxis = lineChart.categoryAxis;
        categoryAxis.parseDates = true; // 由于我们的数据是基于日期的，所以我们将解析日期设置为true
        categoryAxis.minPeriod = "DD"; // 我们的数据是每日的，所以我们将最小时间设置为DD。
        categoryAxis.dashLength = 1; //图表内竖线密度
        categoryAxis.gridAlpha = 0.15;//图表内竖线颜色深浅度
        categoryAxis.axisColor = "#DADADA";//X轴线颜色

        // Y轴
        var valueAxis = new AmCharts.ValueAxis();
        valueAxis.title = "成品粉产量曲线";
        valueAxis.axisColor = "#DADADA";//Y轴线颜色
        valueAxis.dashLength = 1;//图表内横线密度
        valueAxis.logarithmic = true; // this line makes axis logarithmic
        lineChart.addValueAxis(valueAxis);

        // 便利json键
        for (var i = 0; i < materials.length; i++) {
            // 折线添加
            addGraph(materials[i].Value, materials[i].Key);
        }
        

        // 光标线
        var chartCursor = new AmCharts.ChartCursor();
        chartCursor.cursorPosition = "mouse";
        chartCursor.categoryBalloonDateFormat = "YYYY年MM月DD日";
        lineChart.addChartCursor(chartCursor);

        // 是否显示区间条  顶端  只能显示条折线的数据源
        var chartScrollbar = new AmCharts.ChartScrollbar();
        //chartScrollbar.graph = graph;//区间条数据源
        chartScrollbar.scrollbarHeight = 30;//区间条高度
        lineChart.addChartScrollbar(chartScrollbar);

        lineChart.creditsPosition = "bottom-left";//图表水印文字位置


        // 折线图表下方的折线名称
        var legend = new AmCharts.AmLegend();
        legend.useGraphSettings = true; //使用图形设置  
        lineChart.addLegend(legend);

        // 图表写入 div容器
        lineChart.write("lineChartDiv");
    }
    //执行图表读取
    AmCharts.ready(initChart());

    
}

/**
 * 刷新图表
 * @param {} timeSegment 时间段  1 近一个月 3 一个季度 6 半年 12 一年
 * @returns {} 
 */
function refurbishLineChart(element, timeSegment) {
    $(element).parent().parent().siblings('a').html($(element).text() + ' <span class="fa fa-angle-down"></span>');
    $(element).parent().addClass("active").siblings().removeClass("active");
    fnGetLineData(timeSegment, function (data) {
        var materials = data[0].Materials;
        var jsonData = generateChartData(timeSegment, data);
        lineChartmodel(jsonData, materials);
    });
}

/**
 * 获取数据源
 * @param {} timeSegment 
 * @param {} callback 
 * @returns {} 
 */
function fnGetLineData(timeSegment, callback) {
    $.ajax({
        url: formatUrl("/Home/GetLineData"),
        data: {
            timeSegment: timeSegment
        },
        type: "post",
        dataType: "json",
        success: function (data) {
            if (callback != null) {
                callback(data);
            }

        }
    });
}

/**
 * 加载图表数据 
 * @returns {} 返回图表数据源 
 */
function generateChartData(timeSegment, data) {
    var lineChartData = [];
    //结束时间取当前时间
    var endDate = new Date();
    //开始时间  取当前前 timeSegment 个月
    var startDate = new Date();
    startDate.addMonth(-timeSegment);
    for (var i = 0; startDate.addDays(i) <= endDate; i = 1) {

        var json = { "date": startDate.format("yyyy-MM-dd") }
    
        for (var j = 0; j < data.length; j++) {
            if (startDate.format("yyyy-MM-dd") == data[j].Date) {
                if (data[j].Number != null) {
                    json[data[j].Number] = data[j].ActualPhysicalQuantity;
                }

            }
        }

        lineChartData.push(json);
    }
    return lineChartData;
}

/**
 * 页面初始化加载最近一个月数据
 */
refurbishLineChart(null,1);