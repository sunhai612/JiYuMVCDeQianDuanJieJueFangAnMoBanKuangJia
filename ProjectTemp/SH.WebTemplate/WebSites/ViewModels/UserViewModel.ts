﻿/// <reference path="../scripts/typings/knockout/knockout.d.ts" />
/// <reference path="../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../scripts/typings/linq/linq.d.ts" />


/**
 * 视图模型
 */
class UserViewModel {
    userName: KnockoutObservable<string>;
    passWord: KnockoutObservable<string>;

    constructor() {
        this.userName = ko.observable("");
        this.passWord = ko.observable("");
        //验证控件初始化
        $.simpleValidate.init($(".login_frame"));

    }


    /**
     * 密码回车登录
     * @param element 文本框
     */
    eventKeyUp(element) {
        //获取按键值
        var keycode = WebUtil.getKeyCode();
        if (keycode === 13 || keycode === 108) {
            //文本框失去焦点避免重复登录
            $(element).blur();
            this.eventLogin();
        }
    }

    /**
     * 登录实现方法
     */
    fnLogin() {
        WebUtil.ajax({
            url: formatUrl("/User/UserLogin"),
            data: {
                userName: this.userName(),
                passWord: this.passWord()
            },
            type: "post",
            success(result) {
                $("#btn_login").attr("disabled", "disabled");
                location.href = "/Home/Index";
            }
        });
        //重置验证控件
        $.simpleValidate.reset($(".login_frame"));
    }

    /**
     * 登录事件
     */
    eventLogin() {
        if (!$(".login_frame").simpleValidate()) {
            return;
        }
        //进行登录操作
        this.fnLogin();
    }
}
