﻿
/**
 * 用户管理视图模型
 */
class UserManageViewModel {
    modalCreateUser: popups;//创建用户模态
    modalUserResetPassword: popups;//重置密码模态
    modalSetRoles: popups;//给用户分配角色模态
    /**
     * 关键字
     */
    keywords: KnockoutObservable<string>;
    /**
     * 信息系统编号
     */
    systemNo: KnockoutObservable<string>;
    /**
     * 关键字  - 分配权限用
     */
    shSystemNo: KnockoutObservable<string>;
    /**
     * 信息系统编号 - 分配权限用
     */
    shKeywords: KnockoutObservable<string>;
    /**
     * 用户列表
     */
    listUser: KnockoutObservableArray<UserModel>;
    /**
     * 用户模型实体
     */
    userModel: KnockoutObservable<UserModel>;
    /**
     * 角色列表
     */
    listRole: KnockoutObservableArray<RoleModel>;
    /**
     * 用户角色列表
     */
    listUserRole: KnockoutObservableArray<RoleModel>;
    /**
     * 选择的角色ID集
     */
    checkRoles: KnockoutObservableArray<string>;
    /**
     * 确认密码
     */
    secondPwd: KnockoutObservable<string>;
    /**
     * 信息系统字典
     */
    dicSystem: KnockoutObservableArray<KeyValuePair2<string, string>>;
    constructor(dicSystem) {
        var self = this;
        self.modalCreateUser = $("#modalCreateUser").popModal({
            listeners: {
                show() { },
                hide() { }
            },
            width: 500
        });
        self.modalUserResetPassword = $("#modalUserResetPassword").popModal({
            listeners: {
                show() { },
                hide() { }
            },
            width: 500
        });
        self.modalSetRoles = $("#modalSetRoles").popModal({
            listeners: {
                show() { },
                hide() { }
            },
            width: 1000
        });
        self.keywords = ko.observable("");
        self.systemNo = ko.observable("");
        self.shKeywords = ko.observable("");
        self.shSystemNo = ko.observable("");
        self.dicSystem = ko.observableArray<KeyValuePair2<string, string>>(dicSystem);
        self.listUser = ko.observableArray([]).extend({
            paging: {
                pageIndex: 1,
                pageSize: 10,
                total: 0,
                callback: function () {
                    self.fnPageLoad();
                }
            }
        });

        self.userModel = ko.observable(new UserModel());
        self.secondPwd = ko.observable("");

        self.listRole = ko.observableArray([]).extend({
            paging: {
                pageIndex: 1,
                pageSize: 6,
                total: 0,
                callback: function () {
                    self.fnGetRolesByPage();
                }
            }
        });
        self.listUserRole = ko.observableArray([]);
        self.checkRoles = ko.observableArray([]);

        self.init();
    }


    /**
     * 数据加载
     */
    fnPageLoad() {
        var self = this;
        WebUtil.ajax({
            url: formatUrl("/Identity/GetUsers"),
            data: {
                systemNo: self.systemNo(),
                keywords: self.keywords(),
                pageIndex: self.listUser.pageIndex(),
                pageSize: self.listUser.pageSize()
            },
            type: "post",
            dataType: "json",
            success: function (data) {
                self.listUser.removeAll();
                if (data.Datas != null) {
                    self.listUser.SetPageTotal(data.RowCount);
                    for (let item of data.Datas) {
                        self.listUser.push(item);
                    }
                }
            }
        });
    }
    /**
     * 分页获取角色列表
     */
    fnGetRolesByPage() {
        var self = this;
        WebUtil.ajax({
            url: formatUrl("/Identity/GetRolesByPage"),
            data: {
                systemNo: self.shSystemNo(),
                keywords: self.shKeywords(),
                pageIndex: self.listRole.pageIndex(),
                pageSize: self.listRole.pageSize()
            },
            type: "post",
            dataType: "json",
            success: function (data) {
                self.listRole.removeAll();
                if (data.Datas != null) {
                    self.listRole.SetPageTotal(data.RowCount);
                    for (let item of data.Datas) {
                        self.listRole.push(item);
                    }
                }
            }
        });
    }
    /**
     * 获取用户角色
     */
    fnGetRoles(loginId, systemNo) {
        var self = this;
        WebUtil.ajax({
            url: formatUrl("/Identity/GetRoles"),
            data: {
                loginId: loginId,
                systemNo: systemNo
            },
            type: "post",
            dataType: "json",
            success: function (data) {
                self.listUserRole.removeAll();
                for (var item of data) {
                    self.listUserRole.push(item);
                }
                self.checkRoles.removeAll();
                for (var item of data) {
                    self.checkRoles.push(item.Id);
                }
            }
        });
    }
    /**
     * 创建用户
     */
    fnCreateUser() {
        var self = this;
        debugger
        WebUtil.ajax({
            url: formatUrl("/Identity/CreateUser"),
            data: {
                loginId: self.userModel().LoginId(),
                realName: self.userModel().Name(),
                password: self.userModel().PassWord()
            },
            type: "post",
            dataType: "json",
            success: function (data) {
                self.fnPageLoad();
                self.modalCreateUser.hide();
            }
        });
    }
    /**
     * 给用户分配角色
     */
    fnSetRoles() {
        var self = this;
        WebUtil.ajax({
            url: formatUrl("/Identity/SetRoles"),
            data: {
                loginId: self.userModel().LoginId(),
                roleIds: self.checkRoles()
            },
            type: "post",
            dataType: "json",
            success: function (data) {
                self.modalSetRoles.hide();
            }
        });
    }
    /**
     * 重置密码
     * @param loginId
     */
    fnResetPassword() {
        var self = this;
        WebUtil.ajax({
            url: formatUrl("/Identity/ResetPassword"),
            data: {
                loginId: self.userModel().LoginId(),
                newPassword:self.userModel().PassWord()
            },
            type: "post",
            dataType: "json",
            success: function (data) {
                self.modalUserResetPassword.hide();
            }
        });
    }
    /**
     * 启用
     * @param loginId
     */
    fnEnableUser(loginId) {
        var self = this;
        WebUtil.ajax({
            url: formatUrl("/Identity/EnableUser"),
            data: {
                loginId: loginId
            },
            type: "post",
            dataType: "json",
            success: function (data) {
                self.fnPageLoad();
            }
        });
    }
    /**
     * 停用
     * @param loginId
     */
    fnDisableUser(loginId) {
        var self = this;
        WebUtil.ajax({
            url: formatUrl("/Identity/DisableUser"),
            data: {
                loginId: loginId
            },
            type: "post",
            dataType: "json",
            success: function (data) {
                self.fnPageLoad();
            }
        });
    }
    /**
     * 删除
     * @param loginId
     */
    fnRemoveUser(loginId) {
        var self = this;
        WebUtil.ajax({
            url: formatUrl("/Identity/RemoveUser"),
            data: {
                loginId: loginId
            },
            type: "post",
            dataType: "json",
            success: function (data) {
                self.fnPageLoad();
            }
        });
    }


    /**
     * 搜索
     */
    eventSearch() {
        this.fnPageLoad();
    }
    /**
     * 重置搜索
     */
    eventResetting() {
        this.keywords("");
        this.systemNo("");
        this.fnPageLoad();
    }
    /**
     * 呼出创建用户
     */
    eventCreateUserShow() {
        var self = this;
        self.userModel(new UserModel());
        self.secondPwd("");
        self.modalCreateUser.show();
        //重置验证
        $.simpleValidate.reset($("#modalCreateUser"));
    }

    /**
     * 创建用户
     */
    eventCreateUser() {
        if (!$("#modalCreateUser").simpleValidate()) {
            return;
        }
        var self = this;
        if (self.userModel().PassWord() != self.secondPwd()) {
            UIToastr.show("两次密码输入不一致", "系统提示", { toastType:"warning"});
            return;
        }
        self.fnCreateUser();
    }
    /**
     * 呼出分配角色
     */
    eventSetRolesShow(loginId, systemNo) {
        this.checkRoles.removeAll();
        this.userModel(new UserModel());
        this.userModel().LoginId(loginId);
        this.fnGetRolesByPage();
        this.fnGetRoles(loginId, systemNo);
        this.modalSetRoles.show();
    }
    /**
     * 搜索
     */
    eventSearchRole() {
        this.fnGetRolesByPage();
    }
    /**
     * 重置搜索
     */
    eventResettingRole() {
        this.shKeywords("");
        this.shSystemNo("");
        this.fnGetRolesByPage();
    }
    /**
     * 勾选用户角色
     * @param systemRole
     */
    eventSystemRoleCheck(element,systemRole) {
        if ($(element).attr("checked")) {
            this.listUserRole.push(systemRole);
        } else {
            this.listUserRole.remove(systemRole);
        }
    }
    /**
     * 删除用户角色
     * @param userRole 用户角色
     */
    eventDeleteRole(userRole) {
        this.listUserRole.remove(userRole);
        this.checkRoles.remove(userRole.Id);
    }

    /**
     * 分配角色
     */
    eventSetRoles() {
        this.fnSetRoles();
    }
    /**
     * 呼出重置密码
     * @param loginId
     */
    eventResetPasswordShow(loginId) {
        var self = this;
        self.userModel(new UserModel());
        self.userModel().LoginId(loginId);
        self.secondPwd("");
        self.modalUserResetPassword.show();
        //重置验证
        $.simpleValidate.reset($("#modalUserResetPassword"));
    }
    /**
     * 重置密码
     * @param loginId
     */
    eventResetPassword() {
        if (!$("#modalUserResetPassword").simpleValidate()) {
            return;
        }
        var self = this;
        if (self.userModel().PassWord() != self.secondPwd()) {
            UIToastr.show("两次密码输入不一致", "系统提示", { toastType: "warning" });
            return;
        }
        this.fnResetPassword();
    }
    /**
     * 启用
     * @param loginId
     */
    eventEnableUser(loginId) {
        var self = this;
        sh.confirm("确定要启用此用户吗？", function() {
            self.fnEnableUser(loginId);
        });
    }
    /**
     * 停用
     * @param loginId
     */
    eventDisableUser(loginId) {
        var self = this;
        sh.confirm("确定要停用此用户吗？", function () {
            self.fnDisableUser(loginId);
        });
    }
    /**
     * 删除
     * @param loginId
     */
    eventRemoveUser(loginId) {
        var self = this;
        sh.confirm("确定要删除此用户吗？", function () {
            self.fnRemoveUser(loginId);
        });
    }

    init() {
        this.fnPageLoad();
        //验证控件初始化
        $.simpleValidate.init($("#modalCreateUser"));
        $.simpleValidate.init($("#modalUserResetPassword"));
    }
}