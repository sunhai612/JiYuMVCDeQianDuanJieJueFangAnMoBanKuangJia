﻿



class UserResetPasswordViewModel {
    modalResetPassword: popups;
    /**
     * 用户名
     */
    loginId: KnockoutObservable<string>;
    /**
     * 新密码
     */
    newPassword: KnockoutObservable<string>;
    /**
     * 确认密码
     */
    secondPassword: KnockoutObservable<string>;

    constructor(loginId) {
        var self = this;
        self.modalResetPassword = $("#modalResetPassword").popModal({
            listeners: {
                show() { },
                hide() { }
            },
            width: 500
        });
        self.loginId = ko.observable<string>(loginId);
        self.newPassword = ko.observable("");
        self.secondPassword = ko.observable("");

        this.init();
    }
    /**
     * 重置密码
     */
    fnResetPassword() {
        var self = this;
        WebUtil.ajax({
            url: formatUrl("/Identity/ResetPassword"),
            data: {
                loginId: self.loginId(),
                newPassword: self.newPassword()
            },
            type: "post",
            dataType: "json",
            success: function (data) {
                sh.alert("密码已重置");
                self.modalResetPassword.hide();
            }
        });
    }
    /**
     * 呼出重置密码窗体
     */
    eventShow() {
        this.newPassword("");
        this.secondPassword("");
        //重置验证
        $.simpleValidate.reset($("#modalResetPassword"));
        this.modalResetPassword.show();
    }
    /**
     * 重置密码
     */
    evnetResetPassword() {
        if (!$("#modalResetPassword").simpleValidate()) {
            return;
        }
        if (this.newPassword() != this.secondPassword()) {
            UIToastr.show("两次密码输入不一致，请重新输入", "系统提示");
            this.newPassword("");
            this.secondPassword("");
            //重置验证
            $.simpleValidate.reset($("#modalResetPassword"));
            return;
        }
        this.fnResetPassword();
    }

    init() {
        //验证控件初始化
        $.simpleValidate.init($("#modalResetPassword"));
    }
}