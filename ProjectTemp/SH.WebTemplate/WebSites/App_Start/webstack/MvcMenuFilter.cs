﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using $safeprojectname$.WebSites.App_Start.webstack;
using SD.Common.PoweredByLee;
using SD.Infrastructure.Constants;
using SD.Infrastructure.MemberShip;

namespace $safeprojectname$.WebSites.webstack
{
    public class MvcMenuFilter : ActionFilterAttribute
    {
        private readonly bool _isEnable;


        public MvcMenuFilter()
        {
            this._isEnable = true;
        }

        public MvcMenuFilter(bool isEnable)
        {
            this._isEnable = isEnable;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (this._isEnable)
            {
                string actionName = filterContext.RouteData.GetRequiredString("action");
                string controllerName = filterContext.RouteData.GetRequiredString("controller");
                //登录方式  wpf客户端打开时 loginType值为wpf web打开时为null
                string loginType = filterContext.HttpContext.Request["LoginType"];

                if ((actionName == "UserLogin" && controllerName == "User") || (actionName == "Login_Wpf" && controllerName == "User") || (actionName == "Login" && controllerName == "User"))
                {
                    return;
                }

                #region 验证单点登录

                LoginInfo tokenInfo = HttpContext.Current.Session[SessionKey.CurrentUser] as LoginInfo;
                if (tokenInfo == null)
                {
                    if (filterContext.HttpContext.Request.IsAjaxRequest())
                    {
                        //抛出登录失败错误码
                        throw new WebSiteException("1000");
                    }
                    else
                    {
                        if (loginType != null)//wpf客户端登录
                        {
                            HttpContext.Current.Response.Redirect("/User/Login_Wpf");
                        }
                        else
                        {
                            HttpContext.Current.Response.Redirect("/User/Login");
                        }
                        //HttpContext.Current.Response.Redirect("192.168.170.108:9000");
                    }
                    return;
                }

                //登录信息赋值
                SiteInfo.LoginUserInfo = tokenInfo;

                #endregion


                #region 1.0 自动生成菜单处理
                //加载默认路径，通过controller 和 action 匹配。

                string systemNo = SiteInfo.SystemId.ToString();
                if (systemNo.Length == 1)
                    systemNo = "0" + systemNo;
                //添加信息系统菜单缓存
                List<LoginMenuInfo> loginMenuInfos = tokenInfo.LoginMenuInfos.Single(x => x.SystemNo == systemNo).SubMenuInfos.ToList();

                filterContext.Controller.ViewBag.treeData = loginMenuInfos.ToJson();
                filterContext.Controller.ViewBag.loginType = loginType;
                #endregion

                #region  1.1 处理面包屑导航
                if (filterContext.RequestContext.HttpContext.Request.HttpMethod == "GET")
                {
                    //判断参数过来的 是否有 自定义站点路径 
                    if (filterContext.HttpContext.Request.QueryString["sitemap"] == null)
                    {
                        //没有在区域中返回默认目录树 
                        //旧，不支持多级菜单
                        //LoginMenuInfo node = loginMenuInfos.SelectMany(b => b.SubMenuInfos).SingleOrDefault(b => string.Compare(b.Url, "/" + controllerName + "/" + actionName, StringComparison.OrdinalIgnoreCase) == 0);

                        //filterContext.Controller.ViewBag.SiteMapKeys = node.Id!=Guid.Empty ? new[] { loginMenuInfos.Single(b => b.Id == node.ParentId).Name, node.Name }.ToJson() : new string[] { }.ToJson();
                        //新 仅支持三级菜单并有限制
                        string id = filterContext.HttpContext.Request["id"];
                        string url = "/" + controllerName + "/" + actionName;
                        if (id == "Dry" || id == "Wet")
                        {
                            url += "?id=" + id;
                        }
                        List<string> siteMapKeys = new List<string>();
                        siteMapKeys = this.GetSiteMapKeys(1, false, siteMapKeys, loginMenuInfos, url).Value;
                        filterContext.Controller.ViewBag.SiteMapKeys = siteMapKeys.ToJson();
                    } //if
                    else
                    {
                        //没有自定义站点路径 走默认规则
                        //SiteMapKeys在layout上有调用;
                        filterContext.Controller.ViewBag.SiteMapKeys = filterContext.HttpContext.Request.QueryString["sitemap"].Split('~').ToJson();
                    }//elses

                }
                #endregion
            }

        }

        #region # 获取占地地图路径

        /// <summary>
        /// 获取占地地图路径  -- 获取方法有待优化，仅支持三级菜单，多级菜单存在Bug
        /// </summary>
        /// <param name="layer">层级（未使用）</param>
        /// <param name="isFinish">是否完成</param>
        /// <param name="siteMapKeys">站点地图路径</param>
        /// <param name="loginMenuInfos">同级菜单集合</param>
        /// <param name="url">页面地址</param>
        /// <returns></returns>
        private KeyValuePair<bool, List<string>> GetSiteMapKeys(int layer, bool isFinish, List<string> siteMapKeys, List<LoginMenuInfo> loginMenuInfos, string url)
        {
            foreach (LoginMenuInfo info in loginMenuInfos)
            {//记录上级路径
                List<string> oldMapKeys = new List<string>(siteMapKeys);

                siteMapKeys.Add(info.Name);
                if (info.SubMenuInfos.Count == 0)
                {
                    if (info.Url.Trim() == url.Trim())
                    {
                        isFinish = true;
                    }
                }
                else
                {//递归查询菜单
                    KeyValuePair<bool, List<string>> result = this.GetSiteMapKeys(layer + 1, isFinish, siteMapKeys, info.SubMenuInfos.ToList(), url);
                    siteMapKeys = result.Value;
                    isFinish = result.Key;
                }
                //完成成判断
                if (isFinish)
                {
                    break;
                }
                else
                {
                    //未完成清空路径
                    siteMapKeys = new List<string>();
                }
                //将上级路径重新添加
                siteMapKeys.AddRange(oldMapKeys);
            }

            return new KeyValuePair<bool, List<string>>(isFinish, siteMapKeys);
        }

        #endregion
    }
}