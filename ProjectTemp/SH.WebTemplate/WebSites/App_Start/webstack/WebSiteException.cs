﻿using System;

namespace $safeprojectname$.WebSites.App_Start.webstack
{
    public class WebSiteException : Exception
    {
        public WebSiteException(string message) : base(message)
        {

        }

        public WebSiteException()
        {
            
        }

        public WebSiteException(string message, Exception innerException):base(message, innerException)
        {

        }

    }
}