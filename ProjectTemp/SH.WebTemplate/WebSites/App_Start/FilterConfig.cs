﻿using System.Web.Mvc;
using $safeprojectname$.WebSites.webstack;

namespace $safeprojectname$.WebSites
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ExceptionFilter());
            //filters.Add(new MvcMenuFilter(false));
            filters.Add(new MvcMenuFilter());
        }
    }
}
