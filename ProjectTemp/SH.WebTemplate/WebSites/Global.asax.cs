﻿using System.Web.Mvc;
using System.Web.Routing;
using SD.IdentitySystem.LicenseManager.Models;
using SD.IdentitySystem.LicenseManager.Tookits;

namespace $safeprojectname$.WebSites
{
    public class MvcApplication : BaseHttpApplication
    {
        protected override void Application_Start()
        {
            base.Application_Start();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
        /// <summary>
        /// WPF传入的session键值
        /// </summary>
        public const string TargetStorageNo = "targetStorageNo";
        /// <summary>
        /// 授权证书获取
        /// </summary>
        public static readonly License? LicenseMessage = LicenseReader.GetLicense();
    }
}
