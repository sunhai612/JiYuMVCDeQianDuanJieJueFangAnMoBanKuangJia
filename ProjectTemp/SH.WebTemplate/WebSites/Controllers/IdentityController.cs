﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using $safeprojectname$.IBusiness;
using $safeprojectname$.Models;
using SD.Common.PoweredByLee;
using SD.Infrastructure.Attributes;
using SD.Infrastructure.DTOBase;

namespace $safeprojectname$.WebSites.Controllers
{
    public class IdentityController : Controller
    {
        #region 属性/构造

        private readonly IIdentityContract _identityContract;

        public IdentityController(IIdentityContract identityContract)
        {
            this._identityContract = identityContract;
        }

        #endregion

        #region 视图

        #region * 用户管理视图
        [RequireAuthorization("用户管理")]
        public ActionResult UserManager()
        {
            this.ViewBag.dicSystem = this._identityContract.GetInfoSystems().Where(x=>x.Key!="00").OrderBy(x=>x.Value).ToJson();
            return this.View();
        }

        #endregion
        

        #endregion

        #region # 创建用户 —— void CreateUser(string loginId, string realName...

        /// <summary>
        /// 创建用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <param name="realName">真实姓名</param>
        /// <param name="password">密码</param>
        public JsonResult CreateUser(string loginId, string realName, string password)
        {
            this._identityContract.CreateUser(loginId.Trim(), realName.Trim(), password.Trim());
            return null;
        }

        #endregion

        #region # 为用户分配角色 —— void SetRoles(string loginId...

        /// <summary>
        /// 为用户分配角色
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <param name="roleIds">角色Id集</param>
        public JsonResult SetRoles(string loginId, List<Guid> roleIds)
        {
            this._identityContract.SetRoles(loginId, roleIds);
            return null;
        }

        #endregion

        #region # 重置密码 —— void ResetPassword(string loginId, string newPassword)
        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <param name="newPassword">新密码</param>
        public JsonResult ResetPassword(string loginId, string newPassword)
        {
            this._identityContract.ResetPassword(loginId.Trim(), newPassword.Trim());
            return null;
        }
        #endregion

        #region # 启用用户 —— void EnableUser(string loginId)
        /// <summary>
        /// 启用用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        public JsonResult EnableUser(string loginId)
        {
            this._identityContract.EnableUser(loginId);
            return null;
        }
        #endregion

        #region # 停用用户 —— void DisableUser(string loginId)
        /// <summary>
        /// 停用用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        public JsonResult DisableUser(string loginId)
        {
            this._identityContract.DisableUser(loginId);
            return null;
        }
        #endregion

        #region # 删除用户 —— void RemoveUser(string loginId)
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        public JsonResult RemoveUser(string loginId)
        {
            this._identityContract.RemoveUser(loginId);
            return null;
        }
        #endregion


        #region # 分页获取角色列表 —— PageModel<RoleInfo> GetRolesByPage(string systemNo...

        /// <summary>
        /// 分页获取角色列表
        /// </summary>
        /// <param name="systemNo">信息系统编号</param>
        /// <param name="keywords">关键字</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">页容量</param>
        /// <returns>角色列表</returns>
        public JsonResult GetRolesByPage(string systemNo, string keywords, int pageIndex, int pageSize)
        {
            PageModel<RoleModel> roleModels = this._identityContract.GetRolesByPage(systemNo, keywords, pageIndex, pageSize);
            return this.Json(roleModels);
        }

        #endregion

        #region # 获取权限列表字典 —— JsonResult GetAuthorities(string systemNo)

        /// <summary>
        /// 获取权限列表字典
        /// </summary>
        /// <param name="systemNo">信息系统编号</param>
        /// <returns>权限列表</returns>
        public JsonResult GetAuthorities(string systemNo)
        {
            List<KeyValuePair<Guid, string>> authoritys = this._identityContract.GetAuthorities(systemNo);
            return this.Json(authoritys);
        }

        #endregion

        #region # 根据角色获取权限列表 —— JsonResult GetAuthoritiesByRole(...
        /// <summary>
        /// 根据角色获取权限列表
        /// </summary>
        /// <param name="roleId">角色Id</param>
        /// <returns>权限列表</returns>
        public JsonResult GetAuthoritiesByRole(Guid roleId)
        {
            List<KeyValuePair<Guid, string>> authoritys = this._identityContract.GetAuthoritiesByRole(roleId);

            return this.Json(authoritys);
        }
        #endregion

        #region # 分页获取用户列表 —— JsonResult GetUsers(string systemNo...
        /// <summary>
        /// 分页获取用户列表
        /// </summary>
        /// <param name="systemNo">信息系统编号</param>
        /// <param name="keywords">关键字</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">页容量</param>
        /// <returns>用户列表</returns>
        public JsonResult GetUsers(string systemNo, string keywords, int pageIndex, int pageSize)
        {
            PageModel<UserModel> userModels = this._identityContract.GetUsers(systemNo, keywords, pageIndex, pageSize);
            return this.Json(userModels);
        }
        #endregion

        #region # 获取用户角色列表 —— List<RoleModel> GetRoles(string loginId, string systemNo)
        /// <summary>
        /// 获取用户角色列表
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <param name="systemNo">信息系统编号</param>
        /// <returns>角色列表</returns>
        public JsonResult GetRoles(string loginId, string systemNo)
        {
            List<RoleModel> roleModels = this._identityContract.GetRoles(loginId, systemNo);
            return this.Json(roleModels);
        }
        #endregion


        #region # 是否存在用户 —— bool ExistsUser(string loginId)
        /// <summary>
        /// 是否存在用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <returns>是否存在</returns>
        public JsonResult ExistsUser(string loginId)
        {
            return this.Json(!this._identityContract.ExistsUser(loginId));
        }
        #endregion

    }
}