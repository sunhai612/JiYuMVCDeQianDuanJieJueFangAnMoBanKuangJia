﻿using System;
using System.Linq;
using System.Web.Mvc;
using $safeprojectname$.IBusiness;
using SD.IdentitySystem.LicenseManager.Tookits;
using SD.Infrastructure.Constants;
using SD.Infrastructure.MemberShip;

namespace $safeprojectname$.WebSites.Controllers
{
     public class UserController : Controller
    {
        #region 属性
        //身份认证
        private readonly IUserLoginContract _iUserContract;
        #endregion

        #region 构造器

        public UserController(IUserLoginContract iUserContract)
        {
            this._iUserContract = iUserContract;
        }

        #endregion

        #region 登录视图

        public ActionResult Login()
        {
            return this.View();
        }

        #endregion

        #region 登录视图 —— 客户端使用

        public ActionResult Login_Wpf()
        {
            return this.View();
        }

        #endregion

        #region # 登录 —— void Login(string loginId, string password...

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="passWord">密码</param>
        /// <param name="targetStorageNo">由wpf登录使用，暂存仓库编号</param>
        public void UserLogin(string userName, string passWord, string targetStorageNo)
        {
            #region 授权验证

            if (MvcApplication.LicenseMessage != null)
            {
                string uniqueCode = UniqueCode.Compute();
                bool equal = string.Equals(MvcApplication.LicenseMessage.Value.UniqueCode, uniqueCode,
                    StringComparison.CurrentCultureIgnoreCase);

                if (!equal)
                {
                    throw new Exception("许可证授权与本机不匹配，请联系管理员。");
                }
            }
            else
            {
                throw new Exception("系统未授权，请联系管理员。");
            }

            #endregion

            //验证登录
            LoginInfo loginInfo = this._iUserContract.Login(userName.Trim(), passWord.Trim());

            #region 权限验证
            if (loginInfo.LoginMenuInfos.Count == 0)
            {
                throw new Exception("用户无登录权限，请联系管理员。");
            }
            else
            {
                LoginMenuInfo sysInfo = loginInfo.LoginMenuInfos.SingleOrDefault(x => x.SystemNo == "01");
                if (sysInfo == null)
                {
                    throw new Exception("用户无登录权限，请联系管理员。");
                }
            }
            #endregion

            //添加身份认证缓存
            System.Web.HttpContext.Current.Session[SessionKey.CurrentUser] = loginInfo;
            //暂存Wpf接收的仓库ID
            System.Web.HttpContext.Current.Session[MvcApplication.TargetStorageNo] = targetStorageNo;
            System.Web.HttpContext.Current.Session.Timeout = 1440;
        }
        #endregion
    }
}