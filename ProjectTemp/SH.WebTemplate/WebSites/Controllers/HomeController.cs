﻿using System.Web.Mvc;
using $safeprojectname$.IBusiness;

namespace $safeprojectname$.WebSites.Controllers
{
    public class HomeController : Controller
    {
        #region 属性/构造

        private readonly IUserLoginContract _iUserLoginContract;

        public HomeController(IUserLoginContract iUserLoginContract)
        {
            this._iUserLoginContract = iUserLoginContract;
        }

        #endregion

        #region 主页视图

        public ActionResult Index(string id)
        {
            //获取剩余服务时间
            this.ViewBag.ServerLong = (MvcApplication.LicenseMessage.Value.ServiceExpiredDate - DateTime.Now).Days;
            return this.View();
        }

        #endregion

        #region 注销登录
        public ActionResult LogOff()
        {
            this.Session.Clear();
            return this.RedirectToAction("../User/Login");
        }

        #endregion
        

    }
}