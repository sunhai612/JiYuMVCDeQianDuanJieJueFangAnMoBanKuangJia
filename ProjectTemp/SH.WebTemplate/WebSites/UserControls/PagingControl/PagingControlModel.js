﻿(function () {
    'use strict';
    ko.extenders.paging = function (target, options) {
        var defaults = {
            //默认每页10条
            pageSize: 10,
            //当前索引
            pageIndex: 1,
            //总记录数
            total: 0,
            //是否关联反应【页码按钮与页码跳转框】
            isLinkage: true,
            //回调方法
            callback: function () { }
        };
        var oldPageSplitArray = [10, 20, 50];
        var pageSplitArray = [10, 20, 50];

        var parameter = $.extend(defaults, options);
        target.callback = function () {
            parameter.callback();
        };
        target.pageSize = ko.observable(parameter.pageSize);
        if ($.inArray(parameter.pageSize, pageSplitArray) == -1) {
            pageSplitArray.push(parameter.pageSize);
        }

        pageSplitArray.sort(function (x, y) {
            return x - y;
        });

        target.pageSplitArray = ko.observableArray(pageSplitArray);
        target.showSelect = ko.computed(function () {
            return $.inArray(parseFloat(target.pageSize()), oldPageSplitArray) != -1;
        });

        target.pageIndex = ko.observable(parameter.pageIndex);



        target.total = ko.observable(parameter.total);

        target.pageTotal = ko.computed(function () {
            var result = Math.ceil(target.total() / target.pageSize());
            if (result == 0) {
                result = 1;
            }
            return result;
        });
        target.pageSizeChange = function (element) {
            $(element).unbind("change").change(function () {
                if (target.pageSize() != element.value) {
                    target.pageSize(element.value);
                    target.callback();
                }
            });
        };

        target.pageIndexEdit = ko.observable(1);



        //页码超出监听
        target.pageIndexListener = ko.computed(function () {

            if (target.pageIndex() > target.pageTotal()) {
                target.pageIndex(1);
                target.callback();
            }
        }, this);

        target.SetPageTotal = function (total) {
            target.total(total);
            target.pageIndexEdit(target.pageIndex());
        };
        target.IsActive = function (index) {
            if (target.pageIndex() == index)
                return 'active';
            else
                return '';
        };

        target.moveSearch = function () {
            if (!(/^\d{1,}$/.test(target.pageIndexEdit()))) {

                sh.alert({ msg: "请输入正确页数!" });
                return;
            }
            if (target.pageIndexEdit() < 1) {
                sh.alert({ msg: "页数不能小于1!" });

                return;
            }

            if (target.pageIndexEdit() > target.pageTotal() || target.pageIndexEdit() <= 0) {
                sh.alert({ msg: "输入的页数超出总页数!" });
                return;
            }
            target.SetIndex(parseInt(target.pageIndexEdit()));
            target.callback();
        };
        target.movePage = function (index) {

            if (index == target.pageIndex()) {
                return;
            }

            target.SetIndex(index);
            if (parameter.callback != "") {
                target.callback();
            }
        };
        target.movePrevious = function () {
            var index = target.pageIndex() - 1;
            if (index < 1) {
                target.SetIndex(1);
            } else {
                target.SetIndex(index);
            }
            target.callback();
        };
        target.moveNext = function () {
            var index = target.pageIndex() + 1;
            if (index > target.pageTotal) {
                target.SetIndex(target.pageTotal);
            } else {
                target.SetIndex(index);
            }
            target.callback();
        };
        target.SetIndex = function (index) {
            if (parameter.isLinkage) {
                target.pageIndex(index);
            }
            target.pageIndexEdit(index);
        };
        target.pageList = ko.computed(function () {
            var index = parseInt(target.pageIndex());
            var total = parseInt(target.pageTotal());
            var pageNum = 5;//显示页面数目

            if (total < pageNum) {
                pageNum = total;
            }

            //以pageNum 拆分pageTotal 看看一共有多少个组。
            var zuCount = Math.ceil(target.pageTotal() / pageNum);
            //判断当前pageIndex 处在 第几组;
            var curzu = Math.ceil(target.pageIndex() / pageNum) - 1;
            var sourceList = new Array();
            for (var i = 0; i < pageNum; i++) {
                if (curzu * pageNum + i + 1 <= target.pageTotal()) {
                    sourceList[i] = curzu * pageNum + i + 1;
                } else {
                    break;
                }
            }
            return sourceList;
        });
        return target;
    };
}());
function PagingViewModel(params) {
    //#region 属性/变量
    var self = this;
    self.paging = params.paging;
}

if (!ko.components.isRegistered("pagingcontrol")) {
    ko.components.register("pagingcontrol", {
        viewModel: PagingViewModel,
        template: (function () {
            var html = '';
            $.ajax({
                url: formatUrl("/UserControls/PagingControl/PagingControlView.html"),
                data: {},
                type: 'get',
                async: false,
                dataType: 'html',
                success: function (result) {
                    html = result;
                },
                error: function () {

                }
            });
            return html;
        })()
    });
}