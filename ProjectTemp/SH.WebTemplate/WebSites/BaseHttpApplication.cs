﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;

namespace $safeprojectname$.WebSites
{
    /// <summary>
    /// 基础 HTTP 应用程序类。
    /// </summary>
    public abstract class BaseHttpApplication : HttpApplication
    {
        #region 属性定义...

        /// <summary>
        /// 文件块上传记录。
        /// </summary>
        private static Dictionary<Guid, List<int>> _FileChunkUploadLog { get; set; }

        /// <summary>
        /// 错误日志路径。
        /// </summary>
        private static string _ErrorLogPath { get; set; }
        #endregion

        #region 构造
        /// <summary>
        /// 静态构造函数。
        /// </summary>
        static BaseHttpApplication()
        {
            _FileChunkUploadLog = new Dictionary<Guid, List<int>>();
        } 
        #endregion

        #region Application_Start - 应用程序开始事件...

        /// <summary>
        /// 应用程序开始事件。
        /// </summary>
        protected virtual void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            int systemId = Convert.ToInt32(ConfigurationManager.AppSettings["SystemId"]); // 系统编号

            this.Application["SystemId"] = systemId;
            //string rootPath = HttpRuntime.AppDomainAppVirtualPath;
            //if (rootPath.EndsWith("/"))
            //{
            //    rootPath.Remove(rootPath.Length - 1);
            //}
            //string fileHandlerRootUrl = ConfigurationManager.AppSettings["FileHandlerRootUrl"]; // 文件管理根路径
            //string checkRepeatUri = ConfigurationManager.AppSettings["CheckRepeatUri"]; // 检查文件重复路径
            //string uploadUri = ConfigurationManager.AppSettings["UploadUri"]; // 上传文件路径
            //string downLoadUri = ConfigurationManager.AppSettings["DownLoadUri"]; // 下载文件路径（多线程下载支持）
            //string getFileByObjectUri = ConfigurationManager.AppSettings["GetFileByObjectUri"]; // 根据对象编号获取实体文件的路径
            //string getFileByRevisionUri = ConfigurationManager.AppSettings["GetFileByRevisionUri"]; // 根据修订编号获取实体文件的路径
            //if (string.IsNullOrEmpty(fileHandlerRootUrl))
            //{
            //    fileHandlerRootUrl = "/" + rootPath;
            //}
            //else if (fileHandlerRootUrl.StartsWith("~/"))
            //{
            //    fileHandlerRootUrl = fileHandlerRootUrl.Length > 2 ? fileHandlerRootUrl.Substring(2) + rootPath : rootPath;
            //}
            //if (!fileHandlerRootUrl.EndsWith("/"))
            //{
            //    fileHandlerRootUrl = fileHandlerRootUrl.Substring(0, fileHandlerRootUrl.Length - 1);
            //}
            //checkRepeatUri = fileHandlerRootUrl + (string.IsNullOrEmpty(checkRepeatUri) ? "CheckRepeat" : checkRepeatUri);
            //uploadUri = fileHandlerRootUrl + (string.IsNullOrEmpty(uploadUri) ? "Uploads" : (uploadUri.EndsWith("/") ? uploadUri : uploadUri + "/"));
            //downLoadUri = fileHandlerRootUrl + (string.IsNullOrEmpty(downLoadUri) ? "DownLoads" : (downLoadUri.EndsWith("/") ? downLoadUri : downLoadUri + "/"));
            //getFileByObjectUri = fileHandlerRootUrl + (string.IsNullOrEmpty(getFileByObjectUri) ? "Files/O/" : (getFileByObjectUri.EndsWith("/") ? getFileByObjectUri : getFileByObjectUri + "/"));
            //getFileByRevisionUri = fileHandlerRootUrl + (string.IsNullOrEmpty(getFileByRevisionUri) ? "Files/R/" : (getFileByRevisionUri.EndsWith("/") ? getFileByRevisionUri : getFileByRevisionUri + "/"));
            //this.Application["FileHandlerRootUrl"] = fileHandlerRootUrl;
            //this.Application["CheckRepeatUri"] = checkRepeatUri;
            //this.Application["UploadUri"] = uploadUri;
            //this.Application["DownLoadUri"] = downLoadUri;
            //this.Application["GetFileByObjectUri"] = getFileByObjectUri;
            //this.Application["GetFileByRevisionUri"] = getFileByRevisionUri;
            //this.Application["ApplicationRootPath"] = rootPath;
            _ErrorLogPath = this.Server.MapPath("~/Logs/Error.log");
        }
        #endregion

        #region Session_Start - 会话开始事件...

        /// <summary>
        /// 会话开始事件。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Session_Start(object sender, EventArgs e)
        {
            //// Session.Timeout = 60; // 设置 Session 超时为60分钟
            //if (this.Request.Cookies["SessionId"] == null)
            //{
            //    this.Request.Cookies.Add(new HttpCookie("SessionId", this.Session.SessionID));
            //    this.Request.Cookies["SessionId"].Expires = DateTime.Now.AddMinutes(this.Session.Timeout);
            //}
        }
        #endregion

        #region Application_BeginRequest - 开始请求事件...

        /// <summary>
        /// 开始请求事件。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Application_BeginRequest(object sender, EventArgs e)
        {
            //if (this.Request.Cookies["SessionId"] != null)
            //{
            //    this.Request.Cookies["SessionId"].Expires = DateTime.Now.AddMinutes(this.Session.Timeout);
            //}
            //string url = this.Request.Path.ToLower(); // Request.AppRelativeCurrentExecutionFilePath.ToLower();
            //string applicationPath = this.Request.ApplicationPath.ToLower();
            //if (applicationPath[applicationPath.Length - 1] != '/')
            //{
            //    applicationPath += "/";
            //}
            //if (url.StartsWith(applicationPath + "checkrepeat")) // 文件查重 // 测试用链接：http://localhost:8000/HR_Website/CheckRepeat
            //{
            //    #region 文件重复检查处理（秒传支撑）...

            //    #region 获取参数...

            //    int size = Convert.ToInt32(this.Request.Params["size"]); // "2239542"
            //    string md5val = this.Request.Params["md5val"]; // "acb96da0ae6013823e886906c0fb03e5"
            //    #endregion



            //    // 直接返回路径：\"filePath\":\"" + applicationPath + "/Downloads/" + fileId + "\"
            //    return;
            //    #endregion
            //}
            //else if (url.StartsWith(applicationPath + "uploads")) // 文件上传（分块、完整） // 测试用链接：http://localhost:8000/HR_Website/Uploads
            //{
            //    #region 文件/文件块上传处理...

            //    #region 获取参数...

            //    // string id = Request.QueryString["id"]; // "WU_FILE_0"
            //    string id = this.Request.Params["fileId"] == null ? this.Request.Headers["fileId"] : this.Request.Params["fileId"]; // Guid
            //    string fileName = this.Request.Params["name"] == null ? this.Request.Headers["name"] : this.Request.Params["name"]; // "11.bmp"
            //    // string type = Request.QueryString["type"]; // "image/bmp"
            //    // string lastModifiedDate = Request.QueryString["lastModifiedDate"]; // "Thu Oct 29 2015 23:50:11 GMT+0800 (中国标准时间)"
            //    int size = Convert.ToInt32(string.IsNullOrWhiteSpace(this.Request.Params["size"]) ? this.Request.Headers["size"] : this.Request.Params["size"]); // "2239542"
            //    int chunkIndex = Convert.ToInt32(string.IsNullOrWhiteSpace(this.Request.Params["chunk"]) ? this.Request.Headers["chunk"] : this.Request.Params["chunk"]); // "0"
            //    int chunkCount = Convert.ToInt32(string.IsNullOrWhiteSpace(this.Request.Params["chunks"]) ? this.Request.Headers["chunks"] : this.Request.Params["chunks"]); // "3"
            //    int angle = Convert.ToInt32(string.IsNullOrWhiteSpace(this.Request.Params["rotation"]) ? this.Request.Headers["rotation"] : this.Request.Params["rotation"]); // "0" 图片旋转度
            //    string md5val = this.Request.Params["md5val"] == null ? this.Request.Headers["md5val"] : this.Request.Params["md5val"]; // "acb96da0ae6013823e886906c0fb03e5"
            //    string extendName = "." + this.Request.Params["ext"] == null ? this.Request.Headers["ext"] : this.Request.Params["ext"]; // "bmp" => ".bmp"
            //    byte[] chunkBytes = null;
            //    if (this.Request.Files["file"] == null) // || Request.Files.Count == 0)
            //    {
            //        chunkBytes = this.Request.BinaryRead(this.Request.TotalBytes);
            //    }
            //    else
            //    {
            //        Stream stream = this.Request.Files["file"].InputStream;
            //        chunkBytes = new byte[stream.Length];
            //        if (stream.Length > int.MaxValue)
            //        {
            //            this.ReturnData("", "1", "文件已超过最大上传字节（" + int.MaxValue + "）限制！");
            //            return;
            //        }
            //        else
            //        {
            //            stream.Read(chunkBytes, 0, (int)stream.Length);
            //        }
            //        stream.Close();
            //    }

            //    #endregion


            //    #region 保存或获取文件编号...

            //    Guid fileId = Guid.Empty;
            //    if (string.IsNullOrWhiteSpace(id) || !Guid.TryParse(id.Trim(), out fileId))
            //    {
            //        ResultData<FileInfoDTO> saveResultData = UploadHandler.SaveFileInfo(size, md5val, "", "");
            //        if (saveResultData.State == CompositeStateEnum.Success)
            //        {
            //            fileId = saveResultData.Data.Id;
            //        }
            //        else
            //        {
            //            //TODO: 保存文件信息失败，返回
            //            this.ReturnData("", "1", saveResultData.Message);
            //            return;
            //        }
            //    }
            //    #endregion

            //    #region 上传文件或文件块...

            //    ResultData<bool> uploadResultData = UploadHandler.UploadForWeb(fileId, chunkBytes, size, angle, chunkIndex, chunkCount);
            //    chunkBytes = null;
            //    if (uploadResultData.State == CompositeStateEnum.Success)
            //    {
            //        if (uploadResultData.Data)
            //        {
            //            bool uploadAllChunk = false; // 是否已上传所有文件块
            //            lock (_FileChunkUploadLog)
            //            {
            //                if (!_FileChunkUploadLog.ContainsKey(fileId))
            //                {
            //                    _FileChunkUploadLog.Add(fileId, new List<int>());
            //                }
            //            }
            //            lock (_FileChunkUploadLog[fileId])
            //            {
            //                if (!_FileChunkUploadLog[fileId].Contains(chunkIndex))
            //                {
            //                    _FileChunkUploadLog[fileId].Add(chunkIndex);
            //                }
            //                if (_FileChunkUploadLog[fileId].Count() >= chunkCount)
            //                {
            //                    uploadAllChunk = true;
            //                }
            //            }

            //            #region 保存文件修订信息...

            //            if (uploadAllChunk) // 已上传所有文件块，保存文件修订信息
            //            {
            //                ResultData<ObjectRevisionDTO> uploadedResultData = UploadHandler.SaveRevisionInfo(null, Guid.Empty, fileId, fileName, extendName, "", null, "");
            //                if (uploadedResultData.State == CompositeStateEnum.Success)
            //                {
            //                    Guid objectId = uploadedResultData.Data.ObjectId;
            //                    Guid revisionId = uploadedResultData.Data.Id;
            //                    //TODO: 返回修订编号、对象编号
            //                    this.ReturnData("\"objectId\":\"" + objectId.ToString() + "\",\"revisionId\":\"" + revisionId.ToString() + "\"", "0", uploadedResultData.Message);
            //                    return;
            //                }
            //                else
            //                {
            //                    //TODO: 返回保存文件修订信息失败
            //                    this.ReturnData("", "1", uploadedResultData.Message);
            //                    return;
            //                }
            //            }
            //            #endregion

            //            //TODO: 返回文件块上传成功
            //            this.ReturnData("", "0", "文件块上传成功！");
            //            return;
            //        }
            //    }
            //    #endregion

            //    // 直接返回路径：\"filePath\":\"" + applicationPath + "/Downloads/" + fileId + "\"
            //    //TODO: 返回错误信息
            //    this.ReturnData("", "1", uploadResultData.Message);
            //    return;
            //    #endregion
            //}
            //else if (url.StartsWith(applicationPath + "downloads")) // 如果 URL 是应用程序根路径下的 downloads，则为文件请求，多线程下载支持，文件管理器专用 // 测试用链接：http://localhost:8000/HR_Website/DownLoads/1DFF3FC2-06AF-4D50-AE37-C27D65C83323
            //{
            //    //TODO: 多线程下载支持
            //}
            //else if (url.StartsWith(applicationPath + "files")) // 如果 URL 是应用程序根路径下的 files，则为文件请求，一次下载，文件管理器专用 // 测试用链接：http://localhost:8000/HR_Website/Files/O/1DFF3FC2-06AF-4D50-AE37-C27D65C83323 或 http://localhost:8000/HR_Website/Files/R/1DFF3FC2-06AF-4D50-AE37-C27D65C83323
            //{
            //    #region 文件下载处理...

            //    //TODO: 整个文件下载（一般用于网页图片预览等功能）
            //    string param = url.Substring(applicationPath.Length + 6);
            //    string[] paramSplit = param.Split('/');
            //    if (paramSplit.Count() > 1) // 对象编号或修订编号
            //    {
            //        string type = paramSplit[0];
            //        string id = paramSplit[1];
            //        ResultData<FileDownloadDTO> resultData = null;
            //        if (type.Equals("o")) // 对象编号
            //        {
            //            Guid objectId = Guid.Empty;
            //            if (Guid.TryParse(id, out objectId))
            //            {
            //                resultData = DownloadHandler.DownloadByObject(objectId);
            //            }
            //        }
            //        else if (type.Equals("r")) // 修订编号
            //        {
            //            Guid revisionId = Guid.Empty;
            //            if (Guid.TryParse(id, out revisionId))
            //            {
            //                resultData = DownloadHandler.DownloadByRevision(revisionId);
            //            }
            //        }
            //        if (resultData != null && resultData.State == CompositeStateEnum.Success)
            //        {
            //            if (resultData.Data == null) // 文件不存在
            //            {
            //                this.Response.Clear();
            //                this.Response.ClearHeaders();
            //                this.Response.AddHeader("ContentEncoding", "utf-8");
            //                if (this.Response.IsClientConnected)
            //                {
            //                    this.Response.Flush();
            //                }
            //                this.Response.End();
            //                return;
            //            }
            //            this.Response.Clear();
            //            this.Response.ClearHeaders();
            //            // string fileContentType = "application/octet-stream"; // 待替换的文件内容类型，从库查询
            //            this.Response.AddHeader("ContentEncoding", "utf-8");
            //            this.Response.AddHeader("Content-type", resultData.Data.ContentType);
            //            this.Response.AddHeader("Content-Length", resultData.Data.Size.ToString());
            //            this.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + HttpUtility.UrlEncode(resultData.Data.Name) + "\"");
            //            this.Response.BinaryWrite(resultData.Data.FileBytes.ToArray());
            //            if (this.Response.IsClientConnected)
            //            {
            //                this.Response.Flush();
            //            }
            //        }
            //        else // 查询文件失败
            //        {
            //            this.ReturnData("", "1", resultData == null ? "文件“" + id + "”不存在或文件号不正确！" : resultData.Message);
            //        }
            //        resultData = null;
            //        this.Response.End();
            //        return;
            //    }
            //    else // 文件编号
            //    {
            //        this.ReturnData("", "1", "未实现！");
            //    }
            //    return;
            //    #endregion
            //}
            //else
            //{
            //    switch (this.Response.StatusCode)
            //    {
            //        case 200:
            //            break;
            //        case 404:
            //            // 请求的 URL 不存在。
            //            break;
            //        case 500:
            //            break;
            //    }
            //}
        }
        #endregion

        #region ReturnData - 返回数据给前端...

        /// <summary>
        /// 返回数据给前端。
        /// </summary>
        protected virtual void ReturnData(string pData, string pErrorCode, string pMessage)
        {
            //this.Response.ClearContent();
            //this.Response.ContentEncoding = Encoding.UTF8;
            //this.Response.ContentType = "text/plain";
            //// Response.Write("{\"errorCode\":" + pErrorCode + ",\"msg\":\"" + pMessage + "\",\"data\":{\"filePath\":\"123\"}}");
            //string returnMessage = "{\"errorCode\":" + pErrorCode + ",\"msg\":\"" + pMessage + "\",\"data\":{" + pData + "}}";
            //this.Response.Write(returnMessage);
            //if (this.Response.IsClientConnected)
            //{
            //    this.Response.Flush();
            //    this.Response.End();
            //}
            //GC.Collect();
            return;
        }
        #endregion

        #region Session_End - 会话结束时执行...

        /// <summary>
        /// 会话结束时执行。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Session_End(object sender, EventArgs e)
        {
            this.Session.Clear();
            //if (this.Request != null && this.Request.Cookies != null)
            //{
            //    if (this.Request.Cookies["SessionId"] != null)
            //    {
            //        this.Request.Cookies.Remove("SessionId");
            //    }
            //    this.Request.Cookies.Clear();
            //}
            if (this.Response != null && this.Response.IsClientConnected)
            {
                this.Response.Redirect("/User/Login");
            }
        }
        #endregion

        #region Application_Error - 应用程序异常时执行...

        /// <summary>
        /// 应用程序异常时执行。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected virtual void Application_Error(object sender, EventArgs e)
        //{
        //    //Exception exception = Server.GetLastError();
        //    //ExceptionHelper.Process(exception);
        //}
        #endregion
    }
}