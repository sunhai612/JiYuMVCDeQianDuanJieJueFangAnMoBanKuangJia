﻿/**
 * 用户模型
 */
class UserModel {
    /**
     * 用户ID
     */
    Id: KnockoutObservable<string>;
    /**
     * 用户名
     */
    LoginId: KnockoutObservable<string>;
    /**
     * 密码
     */
    PassWord: KnockoutObservable<string>;
    /**
     * 真实姓名
     */
    Name: KnockoutObservable<string>;
    /**
     * 是否启用
     */
    Enable: KnockoutObservable<boolean>;
    /**
     * 创建时间
     */
    CreateTime: KnockoutObservable<string>;

    constructor(model?: any) {
        this.Id = ko.observable(model != null && model.Id != null ? model.Id : emptyGuid);
        this.LoginId = ko.observable(model != null && model.LoginId != null ? model.LoginId : "");
        this.PassWord = ko.observable(model != null && model.PassWord != null ? model.PassWord : "");
        this.Name = ko.observable(model != null && model.Name != null ? model.Name : "");
        this.Enable = ko.observable(model != null && model.Enable != null ? model.Enable : true);
        this.CreateTime = ko.observable(model != null && model.CreateTime != null ? model.CreateTime : "");
    }
}

/**
 * 角色模型
 */
class RoleModel {
    /**
     * 角色ID
     */
    Id: KnockoutObservable<string>;
    /**
     * 角色名称
     */
    Name: KnockoutObservable<string>;
    /**
     * 信息系统编号
     */
    SystemNo: KnockoutObservable<string>;
    /**
     * 信息系统名称
     */
    SystemName: KnockoutObservable<string>;
    /**
     * 角色描述
     */
    Description: KnockoutObservable<string>;
    /**
     * 创建时间
     */
    CreateTime: KnockoutObservable<string>;
    /**
     * 角色关联权限id集
     */
    Authority: KnockoutObservableArray<string>;

    constructor(model?: any) {
        this.Id = ko.observable(model != null && model.Id != null ? model.Id : emptyGuid);
        this.Name = ko.observable(model != null && model.Name != null ? model.Name : "");
        this.SystemNo = ko.observable(model != null && model.SystemNo != null ? model.SystemNo : "");
        this.SystemName = ko.observable(model != null && model.SystemName != null ? model.SystemName : "");
        this.Description = ko.observable(model != null && model.Description != null ? model.Description : "");
        this.CreateTime = ko.observable(model != null && model.CreateTime != null ? model.CreateTime : "");
    }
}