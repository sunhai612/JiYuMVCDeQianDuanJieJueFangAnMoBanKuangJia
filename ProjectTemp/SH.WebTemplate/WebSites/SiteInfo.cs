﻿using System.Configuration;
using SD.Infrastructure.MemberShip;

namespace $safeprojectname$.WebSites
{


    public static class SiteInfo
    {
        /// <summary>
        /// 系统ID
        /// </summary>
        public static string SystemId
        {
            get { return ConfigurationManager.AppSettings["SystemId"]; }
        }


        /// <summary>
        /// 用户登录信息
        /// </summary>
        public static LoginInfo LoginUserInfo { get; set; } = new LoginInfo();

        /// <summary>
        /// 保留小数位数
        /// </summary>
        public static int DigitCapacity = 2;
        /// <summary>
        /// 系统名称
        /// </summary>
        public static string SystemName = "立高生产管理系统(MES)";
    }
}