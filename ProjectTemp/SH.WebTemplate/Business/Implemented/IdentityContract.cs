﻿using System;
using System.Collections.Generic;
using System.Linq;
using $safeprojectname$.IBusiness;
using $safeprojectname$.Models;
using SD.IdentitySystem.IAppService.DTOs.Outputs;
using SD.IdentitySystem.IAppService.Interfaces;
using SD.Infrastructure.DTOBase;

namespace $safeprojectname$.Business.Implemented
{
    public class IdentityContract : IIdentityContract
    {
        #region 属性/构造
        //权限服务接口
        private readonly IAuthorizationContract _iAuthorizationContract;
        //用户服务接口
        private readonly IUserContract _iUserContract;

        public IdentityContract(IAuthorizationContract iAuthorizationContract, IUserContract iUserContract)
        {
            this._iAuthorizationContract = iAuthorizationContract;
            this._iUserContract = iUserContract;
        }

        #endregion

        //命令部分
        #region 命令部分

        #region # 创建用户 —— void CreateUser(string loginId, string realName...
        /// <summary>
        /// 创建用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <param name="realName">真实姓名</param>
        /// <param name="password">密码</param>
        public void CreateUser(string loginId, string realName, string password)
        {
            this._iUserContract.CreateUser(loginId, realName, password);
        }
        #endregion

        #region # 为用户分配角色 —— void SetRoles(string loginId...
        /// <summary>
        /// 为用户分配角色
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <param name="roleIds">角色Id集</param>
        public void SetRoles(string loginId, List<Guid> roleIds)
        {
            this._iUserContract.SetRoles(loginId, roleIds);
        }
        #endregion

        #region # 重置密码 —— void ResetPassword(string loginId, string newPassword)
        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <param name="newPassword">新密码</param>
        public void ResetPassword(string loginId, string newPassword)
        {
            this._iUserContract.ResetPassword(loginId, newPassword);
        }
        #endregion

        #region # 启用用户 —— void EnableUser(string loginId)
        /// <summary>
        /// 启用用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        public void EnableUser(string loginId)
        {
            this._iUserContract.EnableUser(loginId);
        }
        #endregion

        #region # 停用用户 —— void DisableUser(string loginId)
        /// <summary>
        /// 停用用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        public void DisableUser(string loginId)
        {
            this._iUserContract.DisableUser(loginId);
        }
        #endregion

        #region # 删除用户 —— void RemoveUser(string loginId)
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        public void RemoveUser(string loginId)
        {
            this._iUserContract.RemoveUser(loginId);
        }
        #endregion

        #endregion

        //查询部分

        #region 查询部分

        #region # 获取信息系统列表字典 —— List<KeyValuePair<string,string>> GetInfoSystems()
        /// <summary>
        /// 获取信息系统列表字典
        /// </summary>
        /// <returns>信息系统列表</returns>
        public List<KeyValuePair<string, string>> GetInfoSystems()
        {
            IEnumerable<InfoSystemInfo> infoSystemInfos = this._iAuthorizationContract.GetInfoSystems();

            List<KeyValuePair<string, string>> dicSystem = infoSystemInfos.Select(info => new KeyValuePair<string, string>(info.Number, info.Name)).ToList();

            return dicSystem;
        }
        #endregion

        #region # 分页获取角色列表 —— PageModel<RoleModel> GetRolesByPage(string systemNo...
        /// <summary>
        /// 分页获取角色列表
        /// </summary>
        /// <param name="systemNo">信息系统编号</param>
        /// <param name="keywords">关键字</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">页容量</param>
        /// <returns>角色列表</returns>
        public PageModel<RoleModel> GetRolesByPage(string systemNo, string keywords, int pageIndex, int pageSize)
        {
            PageModel<RoleInfo> roleInfos = this._iAuthorizationContract.GetRolesByPage(systemNo, keywords, pageIndex, pageSize);
            List<RoleModel> roleModel = roleInfos.Datas.Where(x => x.Number != "kb").Select(this.ToRoleModel).ToList();
            return new PageModel<RoleModel>(roleModel, pageIndex, pageSize, roleInfos.PageCount, roleInfos.RowCount);
        }
        #endregion

        #region # 获取权限列表字典 —— List<KeyValuePair<Guid,string>> GetAuthorities(string systemNo)
        /// <summary>
        /// 获取权限列表字典
        /// </summary>
        /// <param name="systemNo">信息系统编号</param>
        /// <returns>权限列表</returns>
        public List<KeyValuePair<Guid, string>> GetAuthorities(string systemNo)
        {
            IEnumerable<AuthorityInfo> authorityInfos = this._iAuthorizationContract.GetAuthorities(systemNo);

            List<KeyValuePair<Guid, string>> authoritys =
                authorityInfos.Select(info => new KeyValuePair<Guid, string>(info.Id, info.Name)).ToList();

            return authoritys;
        }
        #endregion

        #region # 根据角色获取权限列表 —— List<KeyValuePair<Guid,string>> GetAuthoritiesByRole(...
        /// <summary>
        /// 根据角色获取权限列表
        /// </summary>
        /// <param name="roleId">角色Id</param>
        /// <returns>权限列表</returns>
        public List<KeyValuePair<Guid, string>> GetAuthoritiesByRole(Guid roleId)
        {
            IEnumerable<AuthorityInfo> authorityInfos = this._iAuthorizationContract.GetAuthoritiesByRole(roleId);

            List<KeyValuePair<Guid, string>> authoritys =
                authorityInfos.Select(info => new KeyValuePair<Guid, string>(info.Id, info.Name)).ToList();

            return authoritys;
        }
        #endregion

        #region # 分页获取用户列表 —— PageModel<UserInfo> GetUsers(string systemNo...
        /// <summary>
        /// 分页获取用户列表
        /// </summary>
        /// <param name="systemNo">信息系统编号</param>
        /// <param name="keywords">关键字</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">页容量</param>
        /// <returns>用户列表</returns>
        public PageModel<UserModel> GetUsers(string systemNo, string keywords, int pageIndex, int pageSize)
        {
            PageModel<UserInfo> userInfos = this._iUserContract.GetUsersByPage(systemNo, keywords, pageIndex, pageSize);
            List<UserModel> userModels = new List<UserModel>();
            foreach (UserInfo info in userInfos.Datas)
            {
                userModels.Add(new UserModel()
                {
                    Id = info.Id,
                    LoginId = info.Number,
                    Name = info.Name,
                    Enable = info.Enabled,
                    CreateTime = info.AddedTime.ToString("yyyy-MM-dd HH:mm:ss")
                });
            }
            return new PageModel<UserModel>(userModels, pageIndex, pageSize, userInfos.PageCount, userInfos.RowCount);
        }
        #endregion

        #region # 是否存在用户 —— bool ExistsUser(string loginId)
        /// <summary>
        /// 是否存在用户
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <returns>是否存在</returns>
        public bool ExistsUser(string loginId)
        {
            return this._iUserContract.ExistsUser(loginId);
        }
        #endregion

        #region # 获取用户角色列表 —— List<RoleModel> GetRoles(string loginId, string systemNo)
        /// <summary>
        /// 获取用户角色列表
        /// </summary>
        /// <param name="loginId">登录名</param>
        /// <param name="systemNo">信息系统编号</param>
        /// <returns>角色列表</returns>
        public List<RoleModel> GetRoles(string loginId, string systemNo)
        {
            IEnumerable<RoleInfo> roleInfos = this._iUserContract.GetUserRoles(loginId, systemNo);
            List<RoleModel> roleModel = roleInfos.Select(this.ToRoleModel).ToList();
            return roleModel;
        }
        #endregion

        #endregion


        #region ====  私有方法 ====

        #region # 角色模型转换  —— RoleModel ToRoleModel(RoleInfo info)
        /// <summary>
        /// 角色模型转换
        /// </summary>
        /// <param name="info">角色输出模型</param>
        /// <returns>角色视图模型</returns>
        private RoleModel ToRoleModel(RoleInfo info)
        {
            RoleModel model = new RoleModel();
            model.Id = info.Id;
            model.Name = info.Name;
            model.SystemNo = info.SystemNo;
            model.SystemName = info.InfoSystemInfo.Name;
            model.Description = info.Description;
            model.CreateTime = info.AddedTime.ToString("yyyy-MM-dd HH:mm:ss");
            return model;
        }

        #endregion

        #endregion
    }
}
