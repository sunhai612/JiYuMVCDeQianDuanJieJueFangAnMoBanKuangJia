﻿using System.Collections.Generic;
using $safeprojectname$.IBusiness;

namespace $safeprojectname$.Business.Implemented
{
    /// <summary>
    /// 枚举获取接口实现
    /// </summary>
    public class EnumContract: IEnumContract
    {
        #region # 获取审核状态枚举集  --  List<KeyValuePair<string, string>> GetCheckStatusList()
        /// <summary>
        /// 获取审核状态枚举集
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetCheckStatusList()
        {
            //return typeof(CheckStatus).GetEnumDescription();\
            return null;
        }
        #endregion
    }
}
