﻿using System.Collections.Generic;
using System.Linq;
using SD.IdentitySystem.IAppService.DTOs.Outputs;
using SD.IdentitySystem.IAppService.Interfaces;
using SD.Infrastructure.MemberShip;
using $safeprojectname$.IBusiness;
using SD.Infrastructure.Constants;

namespace $safeprojectname$.Business.Implemented
{
    public class UserLoginContract : IUserLoginContract
    {
        #region 属性
        //身份认证接口
        private readonly IAuthenticationContract _iAuthenticationContract;
        //用户服务接口
        private readonly IUserContract _iUserContract;
        #endregion

        #region 构造

        public UserLoginContract(IAuthenticationContract iAuthenticationContract,  IUserContract iUserContract)
        {
            this._iAuthenticationContract = iAuthenticationContract;
            this._iUserContract = iUserContract;
        }

        #endregion

        #region # 登录  --  void Login(string loginId,string password,string currentIp)
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="loginId">账号</param>
        /// <param name="password">密码</param>
        public LoginInfo Login(string loginId,string password)
        {
            LoginInfo loginInfo=  this._iAuthenticationContract.Login(loginId.Trim(), password.Trim());
            return loginInfo;
        }

        #endregion
    }
}
