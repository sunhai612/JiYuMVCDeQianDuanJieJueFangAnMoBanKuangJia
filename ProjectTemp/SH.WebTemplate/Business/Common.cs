﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace $safeprojectname$.Business
{
    public static class Common
    {
        #region 枚举值字段
        /// <summary>
        /// 枚举值字段
        /// </summary>
        private const string EnumValueField = "value__"; 
        #endregion


        #region # 获取枚举类型完整信息
        /// <summary>
        /// 获取枚举类型完整信息
        /// </summary>
        /// <param name="enumType">枚举类型</param>
        /// <returns> List KeyValuePair string,string</returns>
        public static List<KeyValuePair<string, string>> GetEnumDescription(this Type enumType)
        {
            #region # 验证参数

            if (!enumType.IsSubclassOf(typeof(Enum)))
            {
                throw new ArgumentOutOfRangeException(string.Format("类型\"{0}\"不是枚举类型！", enumType.Name));
            }

            #endregion

            FieldInfo[] fields = enumType.GetFields();

            ICollection<Tuple<int, string, string>> enumInfos = new HashSet<Tuple<int, string, string>>();

            foreach (FieldInfo field in fields.Where(x => x.Name != EnumValueField))
            {
                DescriptionAttribute enumMember = field.GetCustomAttribute<DescriptionAttribute>();
                int value = Convert.ToInt32(field.GetValue(Activator.CreateInstance(enumType)));

                enumInfos.Add(new Tuple<int, string, string>(value, field.Name, enumMember == null ? field.Name : string.IsNullOrEmpty(enumMember.Description) ? field.Name : enumMember.Description));
            }

            return enumInfos.Select(d => new KeyValuePair<string, string>(d.Item1.ToString(), d.Item3)).ToList();
        } 
        #endregion

        #region # 把str转换为可空的枚举
        /// <summary>
        /// 把str转换为可空的枚举
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static T? ParseNullableEnum<T>(this string str) where T : struct
        {
            return string.IsNullOrEmpty(str) ? null : (T?)Enum.Parse(typeof(T), str);
        } 
        #endregion
        
        #region # 把str转换为可空的枚举

        /// <summary>
        /// 把str转换为可空的枚举
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static T? ParseEnum<T>(this string str) where T : struct
        {
            return string.IsNullOrEmpty(str) ? null : (T?)Enum.Parse(typeof(T), str);
        }

        #endregion
        
        #region # 把str转换为可空的bool

        /// <summary>
        /// 把str转换为可空的bool
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool? ParseNullableBoolean(this string str)
        {
            return string.IsNullOrEmpty(str) ? null : (bool?)bool.Parse(str);
        }

        #endregion

        #region # 把str转换为可空的float

        /// <summary>
        /// 把str转换为可空的float
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static float? ParseNullableFloat(this string str)
        {
            return string.IsNullOrEmpty(str) ? null : (float?)float.Parse(str);
        }

        #endregion
        
        #region # Guid 转换为可空 Guid?
        /// <summary>
        /// Guid 转换为可空 Guid?
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Guid? ParseNullableGuid(this Guid id)
        {
            return id == Guid.Empty ? null : (Guid?)id;
        } 
        #endregion

    }

    public class PageList<T> where T:new ()
    {

        public int Total { get; set; }
        public List<T> Data { get; set; }

        public PageList(IEnumerable<T> data,int total)
        {
            this.Total = total;
            this.Data = data.ToList();
        }
    }
}
